const express = require('express')
var mongoose = require('mongoose');

// MONGOOSE + MONGO CONFIG
// Each schema would typically have it's own file,
// but I've included it here for brevity

const Post = mongoose.model('Post', new mongoose.Schema({
  title: {
    type: String
  },
  message: {
    type: String
  },
}));

// EXPRESS CONFIG
// Routes would be in a seperate file, but I've
// included them here for brevity as well

const app = express()
const port = 3000

app.set('view engine', 'pug')

app.get('/', (req, res) => {
  Post.find({}, (err, docs) => {
    const context = {
      title: 'Fun Post List Example',
      username: 'ezekiel',
      posts: docs
    }
    
    res.render('index', context)
  })
})

mongoose
  .connect('mongodb://localhost/expresstest', {useNewUrlParser: true})
  .then(async () => {
    app.listen(port, () => console.log(`Server listening at http://localhost:${port}`))
  })