1. First, install [yarn](https://yarnpkg.com/en/) (or use `npm` if you've done this before)
2. Second, run `yarn` to install the dependencies
3. Third, run `yarn start` and navigate to localhost:3000 in your browser

Here's what it looks like: [screenshot](https://i.imgur.com/5tyVKl6.png)